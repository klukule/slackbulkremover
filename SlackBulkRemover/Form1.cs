﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SlackBulkRemover
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            richTextBox1.Multiline = true;
            richTextBox1.AppendText("Beginning of log\r\n");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("Running on 5 threads\r\n");
            RUN(0); //RUN
            RUN(1); //FORREST
            RUN(2); //RUN
            RUN(3); //YOU
            RUN(4); //BITCH
        }

        private async void RUN(int tid)
        {
            string token = textBox1.Text;
            bool ok = await SlackAPI.IsTokenValieAsync(token);
            richTextBox1.AppendText("[THREAD "+tid+"] Received valid token: "+ok.ToString() + "\r\n");

            if (ok)
            {
                string uid = await SlackAPI.GetUserID(token);
                richTextBox1.AppendText("[THREAD " + tid + "] User id: " + ok.ToString() + "\r\n");
                bool filesToRemove = true;
                int fcount = 0;
                while (filesToRemove)
                {
                    string[] files = await SlackAPI.GetFiles(token, uid);
                    if (files.Length == 0) { filesToRemove = false; }

                    Debug.WriteLine("-------------------------- " + fcount + " " + files.Length + " --------------------------");
                    foreach (var item in files)
                    {
                        Debug.WriteLine(item);
                        bool success = await SlackAPI.DeleteFile(token, item);
                        if (success)
                        {
                            fcount++;
                            richTextBox1.AppendText("[THREAD " + tid + "] Removed file: " + item + "\r\n");
                        }
                    }
                }
                richTextBox1.AppendText("[THREAD " + tid + "] -------------------------- \r\n[THREAD " + tid + "] THREAD FINISHED\r\n[THREAD " + tid + "] " + fcount+ " files removed\r\n");
                //Proceed
            }
            else
            {
                MessageBox.Show("Invalid token", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
