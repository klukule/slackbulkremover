﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SlackBulkRemover
{
    public static class SlackAPI
    {
        private static readonly string Address = "https://slack.com/api/";
        private static readonly string AuthEndpoint = "auth.test";
        private static readonly string FilesListEndpoint = "files.list";
        private static readonly string FilesDeleteEndpoint = "files.delete";

        private static readonly HttpClient client = new HttpClient();

        public static async Task<bool> IsTokenValieAsync(string token)
        {

            var response = await client.GetAsync(Address + AuthEndpoint + "?token="+token);

            var responseString = await response.Content.ReadAsStringAsync();

            dynamic d = JObject.Parse(responseString);

            return d["ok"];
        }

        public static async Task<string> GetUserID(string token)
        {

            var response = await client.GetAsync(Address + AuthEndpoint + "?token=" + token);

            var responseString = await response.Content.ReadAsStringAsync();

            dynamic d = JObject.Parse(responseString);

            return d["user_id"];
        }

        public static async Task<string[]> GetFiles(string token, string uid)
        {
            var response = await client.GetAsync(Address + FilesListEndpoint + "?token=" + token + "&user="+uid.ToUpper());

            var responseString = await response.Content.ReadAsStringAsync();

            dynamic d = JObject.Parse(responseString);
            List<string> fileIds = new List<string> { };
            foreach (var item in d["files"])
            {
                dynamic i = item["id"];
                fileIds.Add(item["id"].ToString());
            }
            return fileIds.ToArray();
        }

        public static async Task<bool> DeleteFile(string token, string fileId)
        {
            var response = await client.GetAsync(Address + FilesDeleteEndpoint + "?token=" + token + "&file="+fileId);
            var responseString = await response.Content.ReadAsStringAsync();
            dynamic d = JObject.Parse(responseString);
            Debug.WriteLine(responseString);
            return d["ok"];
        }
    }
}
